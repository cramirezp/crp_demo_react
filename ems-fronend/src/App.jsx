
import './App.css'
import EmpleadosComponent from './components/EmpleadosComponent'
import FooterCompenent from './components/FooterCompenent'
import HeaderComponent from './components/HeaderComponent'
import ListEmpleadosComponent from './components/ListEmpleadosComponent'
import ListEmpleadoComponent from './components/ListEmpleadosComponent'
import { BrowserRouter, Routes, Route} from 'react-router-dom'
function App() {


  return (
    <>
    <BrowserRouter>
      <HeaderComponent/>
      <Routes>
        <Route>
          {/* // http://localhost:3000 */}
          <Route path='/' element= {<ListEmpleadoComponent/>}></Route>
          {/* //http://localhost:3000/empleados */}
                
          <Route path='/empleados' element = {<ListEmpleadoComponent/>}></Route>
          {/* //http://localhost:3000/add-empleado */}
          <Route path='/add-empleado' element ={<EmpleadosComponent/>}></Route>
          {/* //http://localhost:3000/edit-empleado/1 */}
          <Route path='edit-empleado/:id' element = {<EmpleadosComponent/>}></Route>

        </Route>
        
      </Routes>
      
      <FooterCompenent/>
    </BrowserRouter>
    </>
  )
}

export default App
