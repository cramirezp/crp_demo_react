import { Button } from 'bootstrap'
import React, { useEffect, useState } from 'react'
import { createEmpleados, getEmpleado, updateEmpleado } from '../services/EmpleadosService'
import { useNavigate, useParams } from 'react-router-dom'
const EmpleadosComponent = () => {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [old, setOld] = useState('')
    const {id} = useParams();
    const [errors, setErrors] = useState({
        firstName:'',
        lastName:'',
        email:'',
        old: ''

    })
const navigator = useNavigate();
// muestra los datos para update
useEffect(()=>{
    if(id){
        getEmpleado(id).then((response) =>{
            setFirstName(response.data.firstName);
            setLastName(response.data.lastName);
            setEmail(response.data.email);
            setOld(response.data.old);
        }).catch(error => {
            console.error(error);
        })
    }
}, [id])
//***************** */

// funcion que guarda los registros nuevos o actualizados
function saveOrUpdateEmpleado(e){


    e.preventDefault();

    if (validatForm()){
        const empleado = {firstName, lastName, email, old}
        console.log(empleado)


        if(id){
            updateEmpleado(id, empleado).then((response) => {
                console.log(response.data);
                navigator('/empleados');
            }).catch(error =>{
                console.error(error);
            })
        }else{
            createEmpleados(empleado).then((response)=>{
                console.log(response.data);
                navigator('/empleados');
        }).catch(error =>{
            console.error(error);
        })
        }

       

       
    }
    
}
//***************** */

// funcion para validar que todas los campos no esten vacios
    function validatForm(){
        let valid = true;
        const errorsCopy = {... errors}

        if (firstName.trim()){
            errorsCopy.firstName = '';
    
        }else{
            errorsCopy.firstName =' Nombre es requerido';
            valid=false;
        }

        if (lastName.trim() ){
            errorsCopy.lastName='';
        }else{
            errorsCopy.lastName='Apellido es requerido';
            valid = false;
        }
        if (email.trim() ){
            errorsCopy.email='';
        }else{
            errorsCopy.email='Email es requerido';
            valid=false;
        }
        if (old ){
            errorsCopy.old='';
        }else{
            errorsCopy.old='Edad es requerida';
            valid= false;
        }

        setErrors(errorsCopy);
        return valid;
    }
//***************** */



// Muestra los titulos dependiendo si es alta o cambios
    function pageTitle(){
        if (id){
            return <h2 className='text-center'> Update Empleado</h2>

        }else{
            return <h2 className='text-center'> Add Empleado</h2>
        }
    }

  return (
    <div className='container'>
        
        <div className='row'>

        
        <div className='card col-md-6 offset-md-3 offset-md-3'>
            {
                pageTitle()
            }
            
            <div className='card-body'>
                <form>
                    <div className='form-group mb-2'>
                        <label className='form-label'> Nombre</label>
                        <input 
                        type ='text'
                        placeholder=' Enter nombre del empleado'
                        name='firstName'
                        value={firstName}
                        className={`form-control ${errors.firstName ? 'is-invalid' : ''}`}
                        onChange={(e) => setFirstName(e.target.value)}
                        >
                            

                        
                        </input>
                        {errors.firstName && <div className='invalid/freedback'> {errors.firstName}</div>}
                    </div>
                </form>
            </div>

            <div className='card-body'>
                <form>
                    <div className='form-group mb-2'>
                        <label className='form-label'> Apellido</label>
                        <input 
                        type ='text'
                        placeholder=' Enter Apellido del empleado'
                        name='lastName'
                        value={lastName}
                        className={`form-control ${errors.lastName ? 'is-invalid' : ''}`}
                        onChange={(e) => setLastName(e.target.value)}
                        >
                            

                        
                        </input>
                        {errors.lastName && <div className='invalid/freedback'> {errors.lastName}</div>}
                    </div>
                </form>
            </div>

            <div className='card-body'>
                <form>
                    <div className='form-group mb-2'>
                        <label className='form-label'> Email</label>
                        <input 
                        type ='email'
                        placeholder=' Enter email del empleado'
                        name='email'
                        value={email}
                        className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                        onChange={(e) =>  setEmail(e.target.value)}
                        >
                            

                        
                        </input>
                        {errors.email && <div className='invalid/freedback'> {errors.email}</div>}
                    </div>
                </form>
            </div>

            <div className='card-body'>
                <form>
                    <div className='form-group mb-2'>
                        <label className='form-label'> Edad</label>
                        <input 
                        type ='number'
                        placeholder=' Enter la edad del empleado'
                        name='old'
                        value={old}
                        className={`form-control ${errors.old ? 'is-invalid' : ''}`}
                        onChange={(e) => setOld(e.target.value)}
                        >
                            

                        
                        </input>
                        {errors.old && <div className='invalid/freedback'> {errors.old}</div>}
                    </div>

                    <button className='btn btn-success'onClick={saveOrUpdateEmpleado}>Submit</button>
                    
                </form>
            </div>

        </div>


        </div>

        
    </div>
  )
}

export default EmpleadosComponent