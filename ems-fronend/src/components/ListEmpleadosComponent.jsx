import React, {useEffect, useState} from 'react';
import { deleteEmpleados, listEmpleados } from '../services/EmpleadosService';
import { Button } from 'bootstrap';
import { useNavigate } from 'react-router-dom';
const ListEmpleadosComponent = () => {

    const [empleados, setEmpleados] = useState([])
    const navigator = useNavigate();
    useEffect(()=>
    {
        getAllEmpleado();
    }, [])

    function getAllEmpleado(){
        listEmpleados().then((response)=>{
            setEmpleados(response.data);
           }).catch(error =>{
            console.error(error);
           })
    }

    function addNewEmpleado(){
        navigator('/add-empleado')
    }

    function updateEmpleado(id){
        navigator(`/edit-empleado/${id}`)
    }

    function removeEmpleado(id){
        console.log(id);
        deleteEmpleados(id).then((response)=>{
            getAllEmpleado();
        }).catch(error =>{
            console.error(error);
        })
    }
    
    return (
        <div>
            <h2 className='text-center'>Lista de empleados</h2>
            <button class="btn btn-primary mb-2" onClick={addNewEmpleado}>Add Empleado</button>

            <table className='table table-striped table-bordered'>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                        <th>Edad</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        empleados.map(empleado =>
                            <tr key={empleado.id}>
                                <td>{empleado.id}</td>
                                <td>{empleado.firstName}</td>
                                <td>{empleado.lastName}</td>
                                <td>{empleado.email}</td>
                                <td>{empleado.old}</td>
                                <td>
                                    <button className='btn btn-info' onClick={()=>updateEmpleado(empleado.id)}>Update</button>
                                    <button className='btn btn-danger' onClick={() => removeEmpleado(empleado.id)}
                                    style={{marginLeft:'10px'}}
                                    >Eliminar</button>
                                </td>
                            </tr>)
                    }

                </tbody>
            </table>

        </div>
    );
};

export default ListEmpleadosComponent;