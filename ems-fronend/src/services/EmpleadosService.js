import axios from "axios";

const REST_API_BASE_URL = 'http://localhost:8080/api/empleados';

export const listEmpleados = () =>  axios.get(REST_API_BASE_URL);

export const createEmpleados = (empledo) => axios.post(REST_API_BASE_URL, empledo);

export const getEmpleado = (empledoId) => axios.get(REST_API_BASE_URL + '/'+ empledoId);

export const updateEmpleado = (empledoId, empledo) => axios.put(REST_API_BASE_URL +'/' + empledoId, empledo);

export const deleteEmpleados = (empledoId) => axios.delete(REST_API_BASE_URL +'/'+ empledoId);

