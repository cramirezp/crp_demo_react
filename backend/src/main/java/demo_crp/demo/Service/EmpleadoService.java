package demo_crp.demo.Service;

import demo_crp.demo.dto.EmpleadoDto;

import java.util.List;

public interface EmpleadoService {
    EmpleadoDto createEmpleado(EmpleadoDto empleadoDto);

    EmpleadoDto getEmpleadoById(Long empleadoId);

    List<EmpleadoDto> getAllEmpleados();

    EmpleadoDto updateEmpleados(Long empleadoId, EmpleadoDto updateEmpleado);

    void deleteEmpleado(Long empledoId);

}
