package demo_crp.demo.Service.Impl;

import demo_crp.demo.Exception.ResourceNotFoundException;
import demo_crp.demo.Mapper.EmpleadoMapper;
import demo_crp.demo.Model.Empleado;
import demo_crp.demo.Repository.EmpleadoRepository;
import demo_crp.demo.Service.EmpleadoService;
import demo_crp.demo.dto.EmpleadoDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmpleadoServiceImpl implements EmpleadoService {

    private EmpleadoRepository empleadoRepository;

    @Override
    public EmpleadoDto createEmpleado(EmpleadoDto empleadoDto){
        Empleado empleado = EmpleadoMapper.mapToEmpleado(empleadoDto);
        Empleado savedEmpleado = empleadoRepository.save(empleado);
        return EmpleadoMapper.maptoEmpleadoDto(savedEmpleado);


    }

    @Override
    public EmpleadoDto getEmpleadoById(Long empleadoId) {
        Empleado empleado = empleadoRepository.findById(empleadoId)
                .orElseThrow(() -> new ResourceNotFoundException("Empleado no existe  con el id: "+empleadoId));
        return EmpleadoMapper.maptoEmpleadoDto(empleado);
    }

    @Override
    public List<EmpleadoDto> getAllEmpleados() {
        List<Empleado> empleados = empleadoRepository.findAll();
        return empleados.stream().map((empleado -> EmpleadoMapper.maptoEmpleadoDto(empleado)))
                .collect(Collectors.toList());
    }

    @Override
    public EmpleadoDto updateEmpleados(Long empleadoId, EmpleadoDto updateEmpleado) {

        Empleado empleado =  empleadoRepository.findById(empleadoId).orElseThrow(
                () -> new ResourceNotFoundException("Empleado no existe con el id: " + empleadoId)
        );

        empleado.setFirstName(updateEmpleado.getFirstName());
        empleado.setLastName(updateEmpleado.getLastName());
        empleado.setEmail(updateEmpleado.getEmail());
        empleado.setOld(updateEmpleado.getOld());

        Empleado updateEmpleadoObj = empleadoRepository.save(empleado);

        return EmpleadoMapper.maptoEmpleadoDto(updateEmpleadoObj);
    }

    @Override
    public void deleteEmpleado(Long empleadoId) {
        Empleado empleado =  empleadoRepository.findById(empleadoId).orElseThrow(
                () -> new ResourceNotFoundException("Empleado no existe con el id: " + empleadoId)
        );
        empleadoRepository.deleteById(empleadoId);
    }


}
