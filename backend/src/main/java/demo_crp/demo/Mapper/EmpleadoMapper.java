package demo_crp.demo.Mapper;

import demo_crp.demo.Model.Empleado;
import demo_crp.demo.dto.EmpleadoDto;

public class EmpleadoMapper {
    public static EmpleadoDto maptoEmpleadoDto(Empleado empleado){
        return new EmpleadoDto(
                empleado.getId(),
                empleado.getFirstName(),
                empleado.getLastName(),
                empleado.getEmail(),
                empleado.getOld()
        );
    }
    public static Empleado mapToEmpleado(EmpleadoDto empleadoDto){
        return new Empleado(
                empleadoDto.getId(),
                empleadoDto.getFirstName(),
                empleadoDto.getLastName(),
                empleadoDto.getEmail(),
                empleadoDto.getOld()
        );
    }
}
