package demo_crp.demo.Controller;

import demo_crp.demo.Service.EmpleadoService;
import demo_crp.demo.dto.EmpleadoDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@AllArgsConstructor
@RestController
@RequestMapping("/api/empleados")
public class EmpleadoController {

    private EmpleadoService empleadoService;
    // Build add empeado REST API
    @PostMapping
    public ResponseEntity<EmpleadoDto> createEmpleado(@RequestBody EmpleadoDto empleadoDto){
        EmpleadoDto savedEmpleado = empleadoService.createEmpleado(empleadoDto);
        return new ResponseEntity<>(savedEmpleado, HttpStatus.CREATED);

    }

    // build get empleado REST API
    @GetMapping("{id}")
    public ResponseEntity<EmpleadoDto> getEmpleadoById(@PathVariable("id") Long empleadoId){
        EmpleadoDto empleadoDto =  empleadoService.getEmpleadoById((empleadoId));
        return ResponseEntity.ok(empleadoDto);

    }

    //build get all empleado REST API
    @GetMapping
    public ResponseEntity<List<EmpleadoDto>> getAllEmpleados(){
        List<EmpleadoDto> empleados = empleadoService.getAllEmpleados();
        return ResponseEntity.ok(empleados);
    }




    // Build  Update empleado REST API
    @PutMapping("{id}")
    public ResponseEntity<EmpleadoDto> updateEmpleado(@PathVariable("id") Long empleadoId, @RequestBody EmpleadoDto updateEmpleado){
        EmpleadoDto empleadoDto = empleadoService.updateEmpleados(empleadoId, updateEmpleado);
        return ResponseEntity.ok(empleadoDto);
    }

    //Build delate empleado REST API

    @DeleteMapping("{id}")
    public ResponseEntity<String> delateEmpleado(@PathVariable("id") long empleadoId){
        empleadoService.deleteEmpleado((empleadoId));
        return ResponseEntity.ok("Empleado deleted successfully.");
    }

}
