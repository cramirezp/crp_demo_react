package demo_crp.demo.Repository;

import demo_crp.demo.Model.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpleadoRepository extends JpaRepository<Empleado, Long > {

}
